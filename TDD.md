# Test Driven Development (2h 30min)

- Toolchain, runtime, build pipeline, testing framework (15 Min)
- Unit Testing Explanation (30min)
- Unit Testing Hands-on (30min)
- Set-up, Tear down with Hands-on (30min)
- Spy and Mock with Hands-on (45min)


[Slide](./TDD.pptx)
