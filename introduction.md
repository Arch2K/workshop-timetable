# Introduction (1 Hour 30 min)

- 3 Level of competency (Implementation, Architecture, Infrastructure)
- Language Landscape (30 min)
- Development Lifecycle (30 min)
- Agile, as a Greedy algorithm,  and User Stories (20 min)
- Testing (10min)

[Slides](./Introduction.pptx)
