# Task 3 - Deployment Familiarization
This homework to familiarize how applications move into production (deployment)

## Aim
- Deploy your HTTP server you wrote in Golang in a Digital Ocean instance via SCP and SSH


## Steps
- Create SSH Key 
- Login DigitalOcean
- Create a Ubuntu Droplet and link your SSH public key
- SCP your files into droplet
- Remember to change your listening to `0.0.0.0` on port `80`
- Install Golang runtime in droplet
- Run your start command in droplet
- Exit your SSH
- visit your browser via ip as if it is localhost

## Help
- [How to create SSH Key](https://www.digitalocean.com/docs/droplets/how-to/add-ssh-keys/create-with-openssh/)
- [How to add SSH Public key to droplet](https://www.digitalocean.com/docs/droplets/how-to/add-ssh-keys/to-account/)
- [How to SSH](https://www.digitalocean.com/docs/droplets/how-to/connect-with-ssh/)
- [Install Go in Droplet](https://www.digitalocean.com/community/tutorials/how-to-install-go-on-ubuntu-18-04)
