# Git, Git Flow and GitLab (3 Hours)

## Basic Git (45min)
- stage
- commit
  - message
  - amend
- checkout
- HEAD concept
- remote
  - add
  - remove
  - fetch 
  - push
  - pull
- log
  - decorate, one-line, graph
- reset
- merge

## Git Lab (45min)
- Set up SSH with GitLab
- Push and pull to GitLab
- Merge and push locally to GitLab

## Git Flow (1h 30min)
- Groups, Projects, Repository
- Dev, Staging  and Prod
- Merge Request
- KanBan board, task noting, task creation
