# Task 2 - HTTP Server Familiarization
This homework is to familiarize how HTTP Server works. We will build a simple server in Nodejs (Javascript) and Golang 

## Aim
- Build a HTTP Server on port 3000 with NodeJS that capitalizes a string you send over
- Build a HTTP Server on port 3001 with Golang that capitalizes a string you send over
- Access via http://localhost:3000 and http://localhost:3001

## Help
- [Golang Tutorial](https://hackernoon.com/how-to-create-a-web-server-in-go-a064277287c9)
- [Node Tutorial](https://expressjs.com/en/starter/installing.html)
- Ask me directly

## Submission
- Send me the code via discord
- Demo how you start the server via screen sharing after I reviewed the code
