# Dependency Injection and Inversion of Control

- Interfaces and Classes (45 min)
- .NET IoC Container (45 min)
   - Singleton
   - Scoped
   - Transient
