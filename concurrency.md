# Concurrency (3 Hour 30min)
- Threads, Concurrency and importance (Level 1 Concurrency) (15 Min)
- Race Condition, Deadlocks & Mutex (15 Min)
- Async Await in C# and Typescript (1 Hour)
- go routine and channels in Golang (1 Hour)
- Process Concurrency (Level 2 Concurrency) (15 Min)
- Actor Model (15 Min)
- Distributed Concurrency (Level 3 Concurrency) (15 Min)
- CAP Theorem (15 Min)
