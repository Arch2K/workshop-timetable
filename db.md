# Database, SQL and ORM (3 Hour)

- Types of Databases and difference - SQL, Object, Graph, In Memory (30min)
- SQL (1 Hour)
  - Postgres on docker demonstration
- ORM and Entity Framework Core (1 Hour 30min)
