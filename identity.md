# Identity (3 Hours)

- Authorization vs Authentication (15 Min)
- Encryption, Encoding and Hashing (Scrpyt, Bycrpt, SHA, Argon)(15 Min)
- SSL (15 Min)
- Session-based Authentication (15 Min)
- JWT-Based Authentication  (2 Hours)
  - Identity Server
  - Application Server
  - Client Side
  - Invalidation
  - Refresh Token
