# HTML, CSS, Javascript and DOM (2 Hours)

- HTML Basics (30 min)
- CSS (1 hour)
  - width, height
  - units - px, %, wv
  - padding, margin, border - border-boxing
  - position - relative, absolute
  - display - block, inline, inline-block
  - transform 
  - flex
- Javascript (30 min)
  - DOM querying
  - DOM class adding
  - DOM event adding
  
  
[Slide](./HTML.pptx)
