# Install Only

### Install WSL Ubuntu

Ensure you have access to all the installation below within your WSL as well.

Click [here](https://docs.microsoft.com/en-us/windows/wsl/install-win10) for tutorial.


### Yarn

Install [yarn](https://yarnpkg.com/en/docs/install#windows-stable).

Ensure yarn is installed

```bash
$ yarn -v
```


### Android Studio with Flutter (For those interested in mobile only)
[Installation](https://developer.android.com/studio)

---
---

# Sign Up Only

### Digital Ocean Account
Create a [Digital Ocean](https://www.digitalocean.com/) account, link your card. Will reimbuirse whatever you used for the project, unless
you used for your own thing.

### Netlify Account
Create a [Netlify](https://app.netlify.com/) account. 

### GitLab Account    
Create a [GitLab](https://gitlab.com) account. 

**Enable 2FA**


---
---

# Install & Sign Up 

### JetBrains
 - WebStorm
 - Rider
 - GoLand
 
 ### Discord
 Install [Discord](https://discordapp.com/)


 ### pCloud 
 Install [pCloud](https://discordapp.com/)


### PostMan
Install [PostMan](https://www.google.com/search?client=firefox-b-d&q=postman)


---
---


# Install & Tutorial + Homework

### Docker

Please install [Docker](https://www.docker.com/get-started) and go through the 
basic tutorial. Do not need to use swarm or compose, the basic `docker run` is good enough

---

### Git

Please install [Git](https://git-scm.com/downloads). Ensure you have access to git via 
the command line

```bash
$ git --version
```

Please watch the following [video](https://www.youtube.com/watch?v=8KCQe9Pm1kg) and have 
an understanding of the basics of Git.

Try to have a basic understanding of:
- `git add`
- `git commit`
- `git add remote`
- `git checkout`
- `git branch`
- `git diff`
- `git merge`

---

### Node.js

##### Installion 
Install Node latest version on your computer [here](https://nodejs.org/en/).

Check the command line that you have installed it

```bash
$ node -v
```

##### Basic Tutorial

**Hello World app in Node.js**

Create a file called `hello.js`

`hello.js`
```js
console.log("Hello world!");
```

Run invoke the node runtime on the file:
```bash
$ node hello.js
```

It should print 
```
Hello World!
```

##### Homework
Create an command line application to that accepts a file and outputs a new file by
capitalizing all the words within the document

---

###  Microsoft Stack

##### Installation and Tutorial
- [Installation](https://dotnet.microsoft.com/learn/dotnet/hello-world-tutorial/install)
- [Tutorial](https://docs.microsoft.com/en-us/dotnet/core/tutorials/using-with-xplat-cli)

##### Homework
Create an command line application to that accepts a file and outputs a new file by
capitalizing all the words within the document


---

### Golang

##### Installation
Install [Golang](https://golang.org/)

##### Homework
Create an command line application to that accepts a file and outputs a new file by
capitalizing all the words within the document

