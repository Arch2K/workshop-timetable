# Linux (2 Hours, Concurrent hands-on)

## Commands 
- !!
- apk-get install
- curl
- touch
- mkdir
- ls
- mkdir
- rm
- cp
- mv

## Tools
- top
- man
- cat
- pipe operator
- PATH
- environment variable (.environment, printenv)
- SSH, private key, public key, SCP

## Final Task
- SSH into Digital Ocean server and run `capitalize.js` from homework
