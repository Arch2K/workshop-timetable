# Task 1 - Frontend Familiarization
This homework is a familiarization of HTML, CSS and Javascript, the holy trinity of cancer in 
Frontend Programming.

## Aim 
- Make a webpage form that asks for
  1. Username
  2. Email
  3. Age
- a HTML `<div>` element that when pressed, prints the username, 
  - prints the username, email and age to console using `console.log`
  - prints `invalid username` if the username is not alphanumeric
  - prints `invalid email` if the email does not follow the format of `<string>@<string>`
  - prints `invalid age` if the age is below 0 and larger than 150
- Make EVERYTHING look nice with CSS
- Align Center for everything
- Add custom fonts
- Make it look nice regardless of resolution (resize your browser window and see if it plays nice) 

## Steps
1. Create a HTML, CSS and Javascript file locally, open the HTML and see if it fulfils the aim above

    > Protip: In chrome, you can see the console and div by inspecting (right-click inspect)

    > Protip: Google is your friend

2. Login to your netlify account and drag the whole folder in to create a new site.
3. Send me the netlify link via discord. 
